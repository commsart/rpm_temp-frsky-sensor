/*
Interrupt driven RPM and temperature sensor for FrSky
*/

#include <FrSkySportSensor.h>
#include <FrSkySportSensorRpmInt.h>
#include <FrSkySportTelemetry.h>
#include <FrSkySportSingleWireSerial.h>
#include <SoftwareSerial.h>

//#define DEBUG
#define LED 13
#define IGNITION_PIN_2  2
#define TEMP_PIN  A3
#define SAMPLES 2  // originally 100, 20, now 2
#define REPORTING 450 // report each 450ms


FrSkySportSensorRpmInt rpm;     // Create RPM sensor with default ID
FrSkySportTelemetry telemetry;  // Create telemetry object without polling
float temp;                     // could use integer, float may be useful if converted to degrees
volatile uint32_t currT0;
volatile uint32_t oldT0;
volatile uint32_t diffT;
volatile uint8_t int0;  // flag signalling that interrupt has been received (active sensor)
uint32_t avr, revs;
uint32_t ct, ot = 0;

void int0vect() {
  // do not worry about micros turning around to small again, managed in reporting
  currT0 = micros();
  diffT = currT0 - oldT0;
  oldT0 = currT0;
  int0 = 1;
}

void setup()  {
  Serial.begin(115200);
  pinMode(LED, OUTPUT);
  currT0 = oldT0 = int0 = 0;
  diffT = 0;
  pinMode(IGNITION_PIN_2, INPUT_PULLUP);  // apply pullup so it does not pickup 50Hz
  pinMode(TEMP_PIN, INPUT);               // analog port to read voltage
  attachInterrupt(0, int0vect, FALLING);  //
  telemetry.begin(FrSkySportSingleWireSerial::SOFT_SERIAL_PIN_4, &rpm);
  #ifdef DEBUG
    Serial.println("Initialised");
  #endif
}

void reportingTime()  {

    digitalWrite(LED, !digitalRead(LED)); // blinking LED each 450ms
    ct = millis();
    
    if ( ct > ot + REPORTING)  {
      
      if (int0)   {
          int0 = 0;
          revs = 60000000/diffT; // revs/minute        
          if (revs > 40000) revs = 0;
      } else  { // no interrupt fired, reset to zero
          diffT = 0;
          revs = 0;        
      }
      temp = analogRead(TEMP_PIN);
      #ifdef DEBUG
        Serial.print(revs);Serial.print("rpm ");Serial.print(temp);Serial.println("deg");
      #endif
      rpm.setData(0, revs);  // shows as RPM[1] in telemetry screen
      rpm.setData(1, temp);  // temp shows as RPM[0]
            
      ot = ct;      
    }
    
}

void loop() {
  reportingTime();
  telemetry.send();  // MAKE TELEMETRY TIMER INTERRUPT DRIVEN SO IT ALWAYS TRANSMITS ON TIME
}
