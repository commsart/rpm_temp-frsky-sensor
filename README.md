## Simple RPM and Temperature sensor in one, compatible with FrSky (pre-ACCESS).

Sensor has been built and tested using arduino mini. Additional components:
- hall effect sensor, powered with +5V out of arduino mini and connected to D1
- thermistor powered with +5V out of arduino mini and connected to GND with ~50kOm resistor. Mid voltage connected to A3. We have used FrSky original temperature sensor but any thermistor could be used
- FrSky S-Port telemetry library (available from other project, older copy included in this project for convenience)
- TX serial port is connected to a socket that should be connected to the RX Smart Port. The board is also powered from this socket and the voltage must not exceed +12V (or whatever the maximum voltage is for your board)

The temperature output reports direct output from the A3 that will range in temperature from 0-1023. Most likely output at room temperature will be hale of this, around 500 and will increase with temperature. If you wish to have the output in degrees, you need to built a mapping function and calibrate the sensor (non-linear!). We are using it to generate alarm if the engine overheats so in the model telemetry setup alarm is raised if the temperature value goes below certain value (need to calibrate it yourself).

The sensor does not listen to input from other FrSky sensor and can only trasmit out its own sensors therefore if use with other sensors it must the the last in the chain (other sensors must be between this sensor and teh RX Smart Port).

D1 and A3 can be changed to any other digitial or analog points, requires changes on the board and in the firmware.

100kOm resitor can and should be changed to match the thermistor resistance at room temperature. It may vary between thermistors. Measure the thermistor resistence at room temperature and use resistor of similar resistance.

Schematics:

![alt text](schematics.png "diagram")

